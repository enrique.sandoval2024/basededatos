/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Vista.JifProductos;
import modelo.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Date;
import java.sql.SQLException;
import javax.swing.JOptionPane;

import java.util.ArrayList;
import com.toedter.calendar.JDateChooser;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

import javax.swing.table.DefaultTableModel;
//import javax.swing.JOptionPane;
/**
 *
 * @author hp
 */
public class Controlador implements ActionListener {
    
    private JifProductos vista;
    
    private dbProducto db;
    private boolean EsActualizar ;
    private int idProducto=0;
    
    public Controlador(JifProductos vista, dbProducto db) {
        this.vista = vista;
        this.db = db;
        
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        
        this.deshabilitar();
        
    }
    
    public void limpiar() {
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.dtfFecha.setDate(null);
        
    }
    
    public void cerrar() {
        int res = JOptionPane.showConfirmDialog(vista, "Desea cerrar el sistema ",
                "Productos", JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (res == JOptionPane.YES_OPTION) {
            vista.dispose();
        }
        
    }
    
    public void habilitar() {
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true); 
        vista.txtPrecio.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnDeshabilitar.setEnabled(true);
        vista.btnHabilitar.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.dtfFecha.setEnabled(true);
        
        
    }
    
    public void deshabilitar() {
        vista.txtCodigo.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.dtfFecha.setEnabled(false);
        vista.btnBuscar.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.btnDeshabilitar.setEnabled(false);
        vista.btnHabilitar.setEnabled(false);
        vista.lista.setEnabled(false);
        
    }
    
    public boolean validar(){
        boolean exito = true;
        if (vista.txtCodigo.getText().equals ("")
          || vista.txtNombre.getText().equals("")
          ||  vista.txtPrecio.getText().equals("")
          || vista.dtfFecha.getDate()== null) exito = false;
            
        return exito;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.btnLimpiar){ this.limpiar();}
        if (ae.getSource() == vista.btnCancelar){ this.limpiar(); this.deshabilitar();}
        if (ae.getSource() == vista.btnCerrar){ this.cerrar();}
        
        if (ae.getSource() == vista.btnNuevo){ this.habilitar(); this.EsActualizar = false;}
        if (ae.getSource() == vista.btnGuardar){
            //VALIDAR
            
            if (this.validar()){
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setNombre(vista.txtNombre.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setStatus(0);
                pro.setFecha(this.convertirAñoMesDia(vista.dtfFecha.getDate()));
                
                
                try{
                if(this.EsActualizar == false){
                    
                    if (db.siExiste(pro.getCodigo())){
                        JOptionPane.showMessageDialog(vista,"Ya existe el Codigo");
                        vista.txtCodigo.requestFocusInWindow();
                    }else {
                        
                    
                // Agregar un nuevo producto
                    db.insertar(pro);
                    JOptionPane.showMessageDialog(vista,"Se agrego con exito el producto ");
                    this.limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());
                    //Actualizar tabla()                  
                    }
                } else{
                    pro.setIdProducto(idProducto);
                    db.actualizar(pro);
                    JOptionPane.showMessageDialog(vista,"Se actualizo con exito el producto ");
                    this.limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());
                    //Actualizar tabla()
                }
                
                } catch (Exception e){
                    JOptionPane.showMessageDialog(vista,"Surgio un error " + e.getMessage());
                    
                }
                
                
                
            }else{
                JOptionPane.showMessageDialog(vista, "Faltaron datos de capturar ");
            }
        
        }
        
        if (ae.getSource() == vista.btnBuscar){
            Productos pro = new Productos();
            
            
            if (vista.txtCodigo.getText().equals("")){
                JOptionPane.showMessageDialog(vista,"Falto capturar el codigo");
                
            } 
            else{
                try {
                pro = (Productos) db.buscar(vista.txtCodigo.getText());
                if(pro.getIdProducto()!=0){
                    idProducto = pro.getIdProducto();
                    vista.txtNombre.setText(pro.getNombre());
                    vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                    convertirStringDate(pro.getFecha());
                    this.EsActualizar = true;
                    vista.btnDeshabilitar.setEnabled(true);
                    vista.btnGuardar.setEnabled(true);
                }else JOptionPane.showMessageDialog(vista, "no se encontro");
                
                
                
                } catch (Exception e){
                JOptionPane.showMessageDialog(vista, "Surgio un error " + e.getMessage());
            }
            }
        }
        
        
            
        
        if (ae.getSource() == vista.btnDeshabilitar){
            
            if (vista.txtCodigo.getText().equals("")){
               JOptionPane.showMessageDialog(vista,"Falto capturar el codigo"); 
            }else{
            int opcion=0;
            opcion = JOptionPane.showConfirmDialog(vista, "¿ Deseas deshabilitar el producto? ","Producto",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE );
            if (opcion == JOptionPane.YES_OPTION){
                Productos pro = new Productos();
                
                
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setIdProducto(idProducto);
                try{
                    db.deshabilitar(pro);
                    JOptionPane.showMessageDialog(vista, "Se deshabilito con exito");
                    this.limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());
                    //Actualizar la tabla;
                    
                    
                    
                }catch (Exception e){
                    JOptionPane.showMessageDialog(vista, "Surgio un error" + e.getMessage());
                }
                
            }
        }
            }
        
        if (ae.getSource() == vista.btnHabilitar){
            if (vista.txtCodigo.getText().equals("")){
               JOptionPane.showMessageDialog(vista,"Falto capturar el codigo"); 
            }else{
            int opcion=0;
            opcion = JOptionPane.showConfirmDialog(vista, "¿ Deseas habilitar el producto? ","Producto",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE );
            if (opcion == JOptionPane.YES_OPTION){
                Productos pro = new Productos();
                
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setIdProducto(idProducto);
                try{
                    db.habilitar(pro);
                    JOptionPane.showMessageDialog(vista, "Se habilito con exito");
                    this.limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());
                    //Actualizar la tabla;
                    
                    
                    
                }catch (Exception e){
                    JOptionPane.showMessageDialog(vista, "Surgio un error" + e.getMessage());
                }
                
            }
        }
         }
        

       

    }

    public void iniciarVista() {
        vista.setTitle("Producto");
        vista.resize(800,500);
        vista.setVisible(true);
        try{
            this.ActualizarTabla(db.lista());
        } catch(Exception e) {
            JOptionPane.showMessageDialog(vista, "Surgio un error" + e.getMessage());
        }
        
        vista.setLocale(null);
        this.deshabilitar();
    }
    
    public String convertirAñoMesDia(Date fecha){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }
    public void convertirStringDate(String fecha){
        try{ 
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(fecha);
            vista.dtfFecha.setDate(date);
        }catch (ParseException e){
            System.err.print(e.getMessage());
        }
        }
    
    
   public void ActualizarTabla(ArrayList<Productos> arr){
            String campos[]= {"idProducto","Codigo","Nombre","Precio","Fecha"};
            
            String[][] datos = new String[arr.size()][5];
            int reglon = 0;
            for(Productos registro : arr){
                datos[reglon][0]= String.valueOf(registro.getIdProducto());
                datos[reglon][1]= registro.getCodigo();
                datos[reglon][2]= registro.getNombre();
                datos[reglon][3]= String.valueOf(registro.getPrecio());
                datos[reglon][4]= registro.getFecha();
                
                
                
                reglon++;
            }
           DefaultTableModel tb = new DefaultTableModel(datos,campos);
           vista.lista.setModel(tb);
        }
    }
        
        
        
    
