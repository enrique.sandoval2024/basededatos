/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Enrique
 */
public class dbProducto extends dbManejador implements Persistencia {

    public dbProducto() {
        super();
    }

    @Override
    public void insertar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;

        String consulta = "";
        consulta = "Insert into productos(codigo,nombre,fecha,precio,status)" + "values(?,?,?,?,?)";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.setString(2, pro.getNombre());
            this.sqlConsulta.setString(3, pro.getFecha());
            this.sqlConsulta.setFloat(4, pro.getPrecio());
            this.sqlConsulta.setInt(5, pro.getStatus());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }

    }

    @Override
    public void actualizar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;

        String consulta = "";
        consulta = "update productos set nombre = ?, fecha = ?, precio = ? where codigo = ? and status=0";
       

        if (this.conectar()) {
             this.sqlConsulta = this.conexion.prepareStatement(consulta);
        
            this.sqlConsulta.setString(1, pro.getNombre());
            this.sqlConsulta.setString(2, pro.getFecha());
            this.sqlConsulta.setFloat(3, pro.getPrecio());
            this.sqlConsulta.setString(4, pro.getCodigo());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }

    }

    @Override
    public void habilitar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;

        String consulta = "";
        consulta = "update productos set status = 0 where codigo = ?";
        

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
        this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }

    }

    @Override
    public void deshabilitar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;

        String consulta = "";
        consulta = "update productos set status = 1 where codigo = ?";
        

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public boolean siExiste(String codigo) throws Exception {
        String consulta = "SELECT 1 FROM productos WHERE codigo = ?";
        boolean exito = false;

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);

            registros = this.sqlConsulta.executeQuery();
            if (registros.next()) {
                exito = true;
                this.desconectar();
            }

        }

        return exito;
    }

    @Override
    public ArrayList lista() throws Exception {
        ArrayList listaProductos = new ArrayList<Productos>();
        String consulta = "select * from productos where status = 0 order by codigo ";
        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            registros = this.sqlConsulta.executeQuery();

            while (registros.next()) {
                Productos pro = new Productos();
                pro.setCodigo(registros.getString("codigo"));
                pro.setNombre(registros.getNString("nombre"));
                pro.setPrecio(registros.getInt("precio"));
                pro.setFecha(registros.getString("fecha"));
                pro.setIdProducto(registros.getInt("idproducto"));
                pro.setStatus(registros.getInt("status"));

                listaProductos.add(pro);
            }
        }

        this.desconectar();
        return listaProductos;

    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        String consulta = "Select * from productos where codigo = ? and status = 0";
        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);

            registros = this.sqlConsulta.executeQuery();

            if (registros.next()) {
                pro.setCodigo(codigo);
                pro.setNombre(registros.getNString("nombre"));
                pro.setPrecio(registros.getInt("precio"));
                pro.setFecha(registros.getString("fecha"));
                pro.setIdProducto(registros.getInt("idproducto"));
                pro.setStatus(registros.getInt("status"));

            }
            this.desconectar();

        }

        return pro;
    }

}
