/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Enrique
 */
public class Productos {
        private int idProducto;
        private String codigo;
        private String nombre;
        private String fecha;
        private float precio;
        private int status; // 0 = Habilitado 1 = Non Habilitado

    public Productos(int idProducto, String codigo, String nombre, String fecha, float precio, int status) {
        this.idProducto = idProducto;
        this.codigo = codigo;
        this.nombre = nombre;
        this.fecha = fecha;
        this.precio = precio;
        this.status = status;
    }
    public Productos() {
        this.idProducto = 0;
        this.codigo = "";
        this.nombre = "";
        this.fecha = "";
        this.precio = 0;
        this.status = 0;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
        
        
        
}
